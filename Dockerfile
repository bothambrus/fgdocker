FROM ubuntu:20.04

RUN set -x \
 && apt-get update \
 && apt-get install -y \
      software-properties-common \
 && apt-add-repository \
      ppa:cantera-team/cantera \
 && apt-get install -y \
      python-is-python3 \
 && apt-get install -y \
      python3 \
      python3-pip \
      python3-venv \
      cantera-python3 \
      cantera-dev \
 && rm -rf /var/lib/apt/lists/*

#RUN set -x \
# && update-alternatives \
#      --install \
#      /usr/bin/python \
#      python \
#      /usr/bin/python2.7 20\
# && update-alternatives \
#      --install \
#      /usr/bin/python \
#      python \
#      /usr/bin/python3.6 10

RUN set -x \
 && python3 -m venv /venv \
 && /venv/bin/pip install --upgrade pip

#
# Install git
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      git \
 && rm -rf /var/lib/apt/lists/*

#
# Add timeset function
#
RUN set -x \
 && cd \
 && pwd \
 && git clone https://gitlab.com/bothambrus/timeset.git 2> /dev/null || (cd timeset ; git pull ; cd ..) \
 && cd timeset \
 && mkdir -p /usr/local/bin/Utils/user \
 && cp /root/timeset/Utils/user/* /usr/local/bin/Utils/user/ \
 && ls /usr/local/bin/Utils/user/ \
 && cd \
 && rm -r timeset

# && git clone https://gitlab.com/bothambrus/timeset.git 2> /dev/null || (cd timeset ; git pull ; cd ..) \
